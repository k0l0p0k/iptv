lang = {
    'en': {
        'name': 'English',
        'strings': {
            'error': 'Error',
            'playerror': 'Playing error',
            'error2': 'Astroncia IPTV error',
            'starting': 'starting',
            'binarynotfound': 'Binary modules not found!',
            'nocacheplaylist': 'Playlist caching off',
            'loadingplaylist': 'Loading playlist...',
            'playlistloaderror': 'Playlist loading error!',
            'playlistloaddone': 'Playling loading done!',
            'cachingplaylist': 'Caching playlist...',
            'playlistcached': 'Playlist cache saved!',
            'usingcachedplaylist': 'Using cached playlist',
            'settings': 'Settings',
            'help': 'Help',
            'channelsettings': 'Channel settings',
            'selectplaylist': 'Select m3u playlist',
            'selectepg': 'Select EPG file (XMLTV or JTV EPG)',
            'selectwritefolder': 'Select folder for recordings and screenshots',
            'deinterlace': 'Deinterlace',
            'channel': 'Channel',
            'savesettings': 'Save settings',
            'm3uplaylist': 'M3U playlist',
            'updateatboot': 'Update\nat launch',
            'epgaddress': 'TV guide\naddress\n(XMLTV or JTV)',
            'udpproxy': 'UDP proxy',
            'writefolder': 'Folder for recordings\nand screenshots',
            'orselectyourprovider': 'Or select\nyour provider',
            'resetchannelsettings': 'Reset channel settings',
            'notselected': 'Not selected',
            'close': 'Close',
            'nochannelselected': 'No channel selected',
            'pause': 'Pause',
            'play': 'Play',
            'exitfullscreen': 'To exit fullscreen mode press',
            'volume': 'Volume',
            'volumeoff': 'Volume off',
            'select': 'Select',
            'tvguide': 'TV guide',
            'startrecording': 'Start recording',
            'loading': 'Loading...',
            'doingscreenshot': 'Doing screenshot...',
            'screenshotsaved': 'Screenshot saved!',
            'screenshotsaveerror': 'Screenshot saving error!',
            'notvguideforchannel': 'No TV guide for channel',
            'preparingrecord': 'Preparing record',
            'nochannelselforrecord': 'No channel selected for record',
            'stop': 'Stop',
            'fullscreen': 'Fullscreen mode',
            'openrecordingsfolder': 'Open recordings folder',
            'record': 'Record',
            'screenshot': 'Screenshot',
            'prevchannel': 'Previous channel',
            'nextchannel': 'Next channel',
            'tvguideupdating': 'Updating TV guide...',
            'tvguideupdatingerror': 'TV guide update error!',
            'tvguideupdatingdone': 'TV guide update done!',
            'recordwaiting': 'Waiting for record',
            'allchannels': 'All channels',
            'favourite': 'Favourites',
            'interfacelang': 'Interface language',
            'tvguideoffset': 'TV guide offset',
            'hours': 'hours',
            'helptext': '''Astroncia IPTV    (c) kestral / astroncia

Cross-platform IPTV player

Supports TV guide (EPG) only in XMLTV and JTV formats!

Hotkeys:

F - fullscreen mode
M - mute
Q - quit program
Space - pause
S - stop playing
H - screenshot
G - tv guide (EPG)
R - start/stop record
P - previous channel
N - next channel
T - open/close channels list
            '''
        }
    },
    'ru': {
        'name': 'Русский',
        'strings': {
            'error': 'Ошибка',
            'playerror': 'Ошибка проигрывания',
            'error2': 'Ошибка Astroncia IPTV',
            'starting': 'запускается',
            'binarynotfound': 'Не найдены бинарные модули!',
            'nocacheplaylist': 'Кэширование плейлиста отключено',
            'loadingplaylist': 'Идёт загрузка плейлиста...',
            'playlistloaderror': 'Ошибка загрузки плейлиста!',
            'playlistloaddone': 'Загрузка плейлиста завершена!',
            'cachingplaylist': 'Кэширую плейлист...',
            'playlistcached': 'Кэш плейлиста сохранён!',
            'usingcachedplaylist': 'Использую кэшированный плейлист',
            'settings': 'Настройки',
            'help': 'Помощь',
            'channelsettings': 'Настройки канала',
            'selectplaylist': 'Выберите m3u плейлист',
            'selectepg': 'Выберите файл телепрограммы (XMLTV / JTV EPG)',
            'selectwritefolder': 'Выберите папку для записи и скриншотов',
            'deinterlace': 'Деинтерлейс',
            'channel': 'Канал',
            'savesettings': 'Сохранить настройки',
            'm3uplaylist': 'M3U плейлист',
            'updateatboot': 'Обновлять\nпри запуске',
            'epgaddress': 'Адрес\nтелепрограммы\n(XMLTV / JTV)',
            'udpproxy': 'UDP прокси',
            'writefolder': 'Папка для записей\nи скриншотов',
            'orselectyourprovider': 'Или выберите\nвашего провайдера',
            'resetchannelsettings': 'Сбросить настройки каналов',
            'notselected': 'не выбрано',
            'close': 'Закрыть',
            'nochannelselected': 'Не выбран канал',
            'pause': 'Пауза',
            'play': 'Воспроизвести',
            'exitfullscreen': 'Для выхода из полноэкранного режима нажмите клавишу',
            'volume': 'Громкость',
            'volumeoff': 'Громкость выкл.',
            'select': 'Выбрать',
            'tvguide': 'Телепрограмма',
            'startrecording': 'Начать запись',
            'loading': 'Загрузка...',
            'doingscreenshot': 'Делаю скриншот...',
            'screenshotsaved': 'Скриншот сохранён!',
            'screenshotsaveerror': 'Ошибка создания скриншота!',
            'notvguideforchannel': 'Нет телепрограммы для канала',
            'preparingrecord': 'Подготовка записи',
            'nochannelselforrecord': 'Не выбран канал для записи',
            'stop': 'Стоп',
            'fullscreen': 'Полноэкранный режим',
            'openrecordingsfolder': 'Открыть папку записей',
            'record': 'Запись',
            'screenshot': 'Скриншот',
            'prevchannel': 'Предыдущий канал',
            'nextchannel': 'Следующий канал',
            'tvguideupdating': 'Обновление телепрограммы...',
            'tvguideupdatingerror': 'Ошибка обновления телепрограммы!',
            'tvguideupdatingdone': 'Обновление телепрограммы завершено!',
            'recordwaiting': 'Ожидание записи',
            'allchannels': 'Все каналы',
            'favourite': 'Избранное',
            'interfacelang': 'Язык интерфейса',
            'tvguideoffset': 'Общая поправка',
            'hours': 'часов',
            'helptext': '''Astroncia IPTV    (c) kestral / astroncia

Кроссплатформенный плеер
для просмотра интернет-телевидения

Поддерживается телепрограмма (EPG)
только в форматах XMLTV и JTV!

Горячие клавиши:

F - полноэкранный режим
M - выключить звук
Q - выйти из программы
Пробел - пауза
S - остановить проигрывание
H - скриншот
G - телепрограмма
R - начать/остановить запись
P - предыдущий канал
N - следующий канал
T - показать/скрыть список каналов
            '''
        }
    }
}